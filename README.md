[[_TOC_]]

## Helpful commands

### MoveIt Setup Assistant

```bash
roslaunch moveit_setup_assistant setup_assistant.launch
```

## Relevant links

### MoveIt

- **MoveIt**: https://docs.ros.org/en/api/moveit_commander/html/annotated.html
- **MoveIt Concepts**: https://moveit.ros.org/documentation/concepts/
- **MoveIt Tutorials**: http://docs.ros.org/en/melodic/api/moveit_tutorials/html/


### MoveIt Reference

- **MoveIt Commander API**: https://docs.ros.org/en/api/moveit_commander/html/annotated.html
- **MoveGroupCommander class**: https://docs.ros.org/en/api/moveit_commander/html/classmoveit__commander_1_1move__group_1_1MoveGroupCommander.html
- **RobotCommander class**: https://docs.ros.org/en/api/moveit_commander/html/classmoveit__commander_1_1robot_1_1RobotCommander.html

### rviz

- **rviz (user guide)**: http://wiki.ros.org/rviz/UserGuide
