import group_interface

interface = group_interface.GroupInterface()

print("")
print("MoveIt group: arm")
print("Current joint states (radians): {}".format(interface.get_joint_state("arm")))
print("Current joint states (degrees): {}".format(interface.get_joint_state("arm", degrees=True)))
print("Current cartesian pose: {}".format(interface.get_cartesian_pose("arm")))

print("")
print("MoveIt group: hand")
print("Current joint states (meters): {}".format(interface.get_joint_state("hand")))
print("Current cartesian pose: {}".format(interface.get_cartesian_pose("hand")))


print("")
print("Planning group: arm")

print("  |-- Reaching named pose...")
interface.reach_named_pose("arm", "home")

print("  |-- Reaching cartesian pose...")
pose = interface.get_cartesian_pose("arm")
pose.position.z += 0.10 
interface.reach_cartesian_pose("arm", pose)

print("  |-- Reaching joint state (radians)...")
interface.reach_joint_state("arm", [0, 0, 0, -1.57, 0, 2.0, 0])
print("  |-- Reaching joint state (degrees)...")
interface.reach_joint_state("arm", [0, 0, 0, -135, 0, 115, 0], degrees=True)


print("")
print("Planning group: hand")

print("  |-- Reaching joint state...")
interface.reach_joint_state("hand", [0.025, 0.025])
interface.reach_joint_state("hand", [0, 0])
